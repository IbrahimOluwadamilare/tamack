<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class route extends Model
{
    //

    protected $fillable = [
        'longitude', 'latitude', 'tripId', 'vehicleId',
    ];
}
