<?php

namespace App\Providers\v1;

use App\Services\v1\DriverRouteService;
use Illuminate\Support\ServiceProvider;

class DriverRouteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(DriverRouteService::class, function ($app) {
            return new DriverRouteService();
        });
    }
}
