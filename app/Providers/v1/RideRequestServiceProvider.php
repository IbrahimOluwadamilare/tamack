<?php

namespace App\Providers\v1;

use App\Services\v1\RideRequestService;

use Illuminate\Support\ServiceProvider;

class RideRequestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(RideRequestService::class, function ($app) {
            return new RideRequestService();
        });
    }
}
