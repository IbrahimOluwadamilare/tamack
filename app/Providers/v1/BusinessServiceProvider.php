<?php

namespace App\Providers\v1;

use App\Services\v1\BusinessService;

use Illuminate\Support\ServiceProvider;

class BusinessServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind(BusinessService::class, function($app){
            return new BusinessService();
        });
    }
}
