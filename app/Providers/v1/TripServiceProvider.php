<?php

namespace App\Providers\v1;

use App\Services\v1\TripService;

use Illuminate\Support\ServiceProvider;

class TripServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(TripService::class, function ($app) {
            return new TripService();
        });
    }
}
