<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicle extends Model
{

    protected $fillable = [
        'vehicleName', 'operatorId', 'vehicleRegistrationNo', 'vehicleTypeId', 'model',
    ];
    //
    public function operatorvehicle()
    {
        return $this->belongsTo('App\operator');
    }
}
