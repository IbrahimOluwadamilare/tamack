<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\v1\DriverRouteService;

class DriverRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $business;
    public function __construct(DriverRouteService $service)
    {
        $this->business = $service;
    }

    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->business->validate($request->all());

        if ($response != "error validating user inputs") {
            $createRoute = $this->business->createRoute($request);
            return response()->json($createRoute, 201);
        } else {
            return response()->json(['message' => 'Error Unable to create a driver route, Please check the input details and try again'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = $this->business->getTripRoute($Id);
        return response()->json($data);
    }


}
