<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\v1\TripService;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $business;
    public function __construct(TripService $service)
    {
        $this->business = $service;
    }


    public function index()
    {
        //
        $parameters = request()->input();

        $data = $this->business->getTrip($parameters);

        return response()->json($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->business->validate($request->all());

        if ($response != "error validating user inputs") {
            $createTrip= $this->business->createTrip($request);
            return response()->json($createTrip, 201);
        } else {
            return response()->json(['message' => 'Error Unable to create a Trip, Please check the input details and try again'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $data = $this->business->getTrip($Id);
        return response()->json($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $updateTrip = $this->business->updateTrip($request, $id);
        return response()->json($updateTrip, 200);
    }


}
