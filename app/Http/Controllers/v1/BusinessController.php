<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\v1\BusinessService;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $business;
    public function __construct(BusinessService $service){
        $this->business = $service;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $response = $this->business->validate($request->all());

        if ($response != "error validating user inputs") {
            $createOperator = $this->business->createOperator($request);
            return response()->json($createOperator, 201);
        } else {
            return response()->json(['message' => 'Error Unable to create a new business Operator, Please check the input details and try again'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($operatorId)
    {
        //

        $data = $this->business->getBusinessOperator($operatorId);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        // $response = $this->business->validate($request->all());

        // if ($response != "error validating user inputs") {
            $updatebusinessOperator = $this->business->updateOperator($request, $id);
            return response()->json($updatebusinessOperator, 200);
        // } else {
        //     return response()->json(['message' => 'Error Unable to Update Business Operator Details'], 500);
        // } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try{
            $deleteBusinessOperator = $this->business->deleteOperator($id);
            return response()->make('', 204);
        } catch (ModelNotFoundException $ex) {
            throw $ex;
        } catch (Exception $e) {
            return response()->json(['message' => 'Error Unable to Delete Business Operator'], 500);
        }
    }
}
