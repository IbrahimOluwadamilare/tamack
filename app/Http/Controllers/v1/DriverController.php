<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\v1\DriverService;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $business;
    public function __construct(DriverService $service)
    {
        $this->business = $service;
    }
    public function index()
    {
        //

        $parameters = request()->input();

        $data = $this->business->getDrivers($parameters);
        return response()->json($data);
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->business->validate($request->all());

        if ($response != "error validating user inputs") {
            $tamarkDriver = $this->business->createDriver($request);
            return response()->json($tamarkDriver, 201);
        } else {
            return response()->json(['message' => 'Error Unable to create a new driver, Please check the input details and try again'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $parameters = request()->input();
        $parameters['driverId'] = $id;
        $data = $this->business->getDrivers($parameters);
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        // $response = $this->business->validate($request->all());

        // if ($response != "error validating user inputs") {
            $updateDriver = $this->business->updateDriver($request, $id);
            return response()->json($updateDriver, 200);
        // } else {
        //     return response()->json(['message' => 'Error Unable to Update Business Details'], 500);
        // } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $deleteDriver = $this->business->deleteDriver($id);
            return response()->make('', 204);
        } catch (ModelNotFoundException $ex) {
            throw $ex;
        } catch (Exception $e) {
            return response()->json(['message' => 'Error Unable to Delete Business'], 500);
        }
    }
}
