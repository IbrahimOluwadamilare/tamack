<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class operatorCard extends Model
{

    protected $fillable = [
        'ccv', 'operatorId', 'cardNo', 'expiryDate',
    ];
    
    public function operatorcarddetails()
    {
        return $this->belongsTo('App\operator');
    }
}
