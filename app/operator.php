<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class operator extends Model
{

    protected $fillable = [
        'companyName', 'password', 'operatorId', 'cacRegNo', 'email', 'phoneNo', 'address', 'city', 'state',
    ];
    
    public function operatorcarddetails()
    {
        return $this->hasOne('App\operatorCard');
    }

    public function operatordrivers()
    {
        return $this->hasMany('App\driver');
    }

    public function operatorvehicle()
    {
        return $this->hasMany('App\vehicle');
    }
}
