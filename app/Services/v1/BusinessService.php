<?php 

namespace App\Services\v1;

use App\operator;
use App\vehicle;
use App\operatorCard;
use Validator;

class BusinessService
{

    // protected $clauseProperties = [
    //     'businessLat',
    //     'businessLong',
    //     'businessId',
    //     'businessCategory',
    //     'businessTags'
    // ];

    protected $rules = [
        'email' => 'required|string|email|max:255|unique:operators',

    ];


    public function validate($operator)
    {
        $validator = Validator::make($operator, $this->rules);

        if ($validator->fails()) {
            return "error validating user inputs";
        } else {
            $validator->validate();
        }
    }

    public function getBusinessOperator ($id){
        $result =  operator::where('operatorId', $id)->firstOrFail();
        return $result;
    }

    public function createOperator($request)
    {

        $unqBusinessId = $this->generateOperatorId();

        $newBusinessOperator = operator::create([
            'companyName' => $request->companyName,
            'operatorId' => $unqBusinessId,
            'cacRegNo' => $request->cacRegNo,
            'email' => $request->email,
            'phoneNo' => $request->phoneNo,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
        ]);

        if ($newBusinessOperator) {

            $newOperatorCardDetails = operatorCard::create([
                'ccv' => $request->ccv,
                'cardNo' => $request->cardNo,
                'expiryDate' => $request->expiryDate,
                'operatorId' => $unqBusinessId,
            ]);

            $newOperatorVehicleRegistration = vehicle::create([
                'vehicleName' => $request->vehicleName,
                'vehicleRegistrationNo' => $request->vehicleRegistrationNo,
                'vehicleTypeId' => $request->vehicleTypeId,
                'model' => $request->model,
                'operatorId' => $unqBusinessId,
            ]);

            if ($newOperatorVehicleRegistration && $newOperatorCardDetails) {
                return $newBusinessOperator;
            } else {
                return "Oops, Operator Card Registration Unssucessful, regisrtration unsucessful Please try again later";
            }
        } else {
            return "Oops, User Registration Unssucessful Please try again later";
        }
    }

    public function updateOperator($request, $id)
    {
        $businessOperator = operator::where('operatorId', $id)->firstOrFail();

        $businessOperator->companyName = $request->input('companyName');
        $businessOperator->cacRegNo = $request->input('cacRegNo');
        $businessOperator->email = $request->input('email');
        $businessOperator->phoneNo = $request->input('phoneNo');
        $businessOperator->password = bcrypt($request->password);
        $businessOperator->address = $request->input('address');
        $businessOperator->city = $request->input('city');
        $businessOperator->state = $request->input('state');

        $businessOperator->save();

        return $businessOperator;

    }

    public function deleteOperator($OperatorId)
    {

        $businessOperator = operator::where('operatorId', $OperatorId)->firstOrFail();


        $businessOperator->delete();
    }

    public function generateOperatorId()
    {

        $unqOperatorId = mt_rand(10000000, 99999999); // better than rand()

        // call the same function if the OperatorId exists already

        $var = $this->OperatorIdExists($unqOperatorId);

        if ($var) {
            return generateOperatorId();
        }

        // otherwise, it's valid and can be used
        return $unqOperatorId;
    }

    public function OperatorIdExists($unqOperatorId)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return operator::where('operatorId', $unqOperatorId)->exists();
    }
}