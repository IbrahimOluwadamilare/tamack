<?php 

namespace App\Services\v1;

use App\driver;
use App\operator;
use App\vehicle;
use Validator;

class DriverService
{
    protected $clauseProperties = [
        'latitude',
        'longitude',
        'driverId',
        'driverStatus',
    ];

    protected $rules = [
        // 'email' => 'required|string|email|max:255|unique:drivers',
        // 'operatorid'=>'required|integer|unique:operators',
        // 'vehicleId'=> 'required|integer|unique:vehicles',
    ];

    public function validate($driver)
    {
        $validator = Validator::make($driver, $this->rules);

        if ($validator->fails()) {
            return "error validating user inputs";
        } else {
            $validator->validate();
        }
    }
    public function getDrivers($parameters)
    {
        if (empty($parameters)) {
            $result = driver::all();
            return $result;
        }

        $whereClauses = $this->getWhereClause($parameters);

        if (isset($whereClauses['latitude']) && isset($whereClauses['longitude'])) {
            $availableDrivers = $this->haversine($whereClauses);
            
            //return available Drivers;
            return $availableDrivers;
        } elseif (isset($whereClauses['driverStatus'])) {
            $availableDrivers = $this->getDriverByStatus($whereClauses);

            return $availableDrivers;
        }

        $driverByStatus = driver::where($whereClauses)->get();

        return $driverByStatus;
    }

    public function createDriver($request)
    {

        $unqBusinessId = $this->generateDriverId();

        $newTamarkDriver = driver::create([
            'firstName' => $request->firstName,
            'driverId' => $unqBusinessId,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'phoneNo' => $request->phoneNo,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
            'routeId' => $request->routeId,
            'status' => $request->status,
            'requestStatus' => $request->requestStatus,
            'pair' => $request->pair,
            'rating' => $request->rating,
            'operatorid' => $request->operatorid,
            'vehicleId' => $request->vehicleId,
        ]);

        if ($newTamarkDriver)
        {
            return $newTamarkDriver;
        } else 
        {
            return "Oops, Driver Registration Unssucessful Please try again later";
        }
    }

    public function updateDriver($request, $id)
    {
        $tamarkDriver = driver::where('driverId', $id)->firstOrFail();

        $tamarkDriver->firstName = $request->input('firstName');
        $tamarkDriver->lastName = $request->input('lastName');
        $tamarkDriver->email = $request->input('email');
        $tamarkDriver->phoneNo = $request->input('phoneNo');
        $tamarkDriver->password = bcrypt($request->password);
        $tamarkDriver->address = $request->input('address');
        $tamarkDriver->city = $request->input('city');
        $tamarkDriver->state = $request->input('state');
        $tamarkDriver->longitude = $request->input('longitude');
        $tamarkDriver->latitude = $request->input('latitude');
        $tamarkDriver->routeId = $request->input('routeId');
        $tamarkDriver->status = $request->input('status');
        $tamarkDriver->requestStatus = $request->input('requestStatus');
        $tamarkDriver->pair = $request->input('pair');
        $tamarkDriver->rating = $request->input('rating');

        $tamarkDriver->save();

        return $tamarkDriver;

    }

    public function deleteDriver($driverId)
    {

        $tamarkDriver = driver::where('driverId', $driverId)->firstOrFail();

        $tamarkDriver->delete();
    }

    public function generateDriverId()
    {

        $unqDriverId = mt_rand(10000000, 99999999); // better than rand()

        // call the same function if the OperatorId exists already

        $var = $this->DriverIdExists($unqDriverId);

        if ($var) {
            return generateDriverId();
        }

        // otherwise, it's valid and can be used
        return $unqDriverId;
    }

    public function DriverIdExists($unqDriverId)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return driver::where('driverId', $unqDriverId)->exists();
    }

    public function getWhereClause($parameters)
    {

        $clause = [];

        foreach ($this->clauseProperties as $prop) {
            if (in_array($prop, array_keys($parameters))) {
                $clause[$prop] = $parameters[$prop];
            }
        }

        return $clause;
    }

    public function getDriverByStatus($whereClauses)
    {
        $businesscategory = $whereClauses['driverStatus'];

        $data = driver::select('*')
            ->where('status', $businesscategory)
            ->get();
        return $data;
    }

    public function haversine($whereClauses, $radius = 300, $max_distance = 300)
    {

        if (isset($whereClauses['driverStatus'])) {
            $latitude = $whereClauses['latitude'];

            $longitude = $whereClauses['longitude'];

            $driverStatus = $whereClauses['driverStatus'];

            $data = driver::select('*')
                ->where('status', $driverStatus)
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                            cos( radians( latitude ) )
                            * cos( radians( longitude ) - radians(?)
                            ) + sin( radians(?) ) *
                            sin( radians( latitude ) ) )
                            ) AS distance', [$latitude, $longitude, $latitude])
                ->havingRaw('distance <= ?', [$max_distance])
                ->orderBy('distance', 'ASC')
                ->get();

            return $data;
        } 
    }

}