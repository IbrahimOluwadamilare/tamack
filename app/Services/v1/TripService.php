<?php 

namespace App\Services\v1;

use App\trip;
use Validator;

class TripService
{
    protected $rules = [
        // 'email' => 'required|string|email|max:255|unique:drivers',
        // 'operatorid'=>'required|integer|unique:operators',
        // 'vehicleId'=> 'required|integer|unique:vehicles',
    ];
    public function validate($request)
    {
        $validator = Validator::make($request, $this->rules);

        if ($validator->fails()) {
            return "error validating user inputs";
        } else {
            $validator->validate();
        }
    }

    public function getTrip($id)
    {
        $result = trip::where('id', $id)->firstOrFail();
        return $result;
    }

    public function createTrip($request)
    {

        $newTrip = trip::create([
            'vehicleId' => $request->vehicleId,
            'driverId' => $request->driverId,
            'customerId' => $request->customerId,
            'status' => $request->status,
        ]);

        if ($newTrip) {
            return $newTrip;
        } else {
            return "Oops, unable to register a trip";
        }
    }

    public function updateTrip($request, $id)
    {
        $trip = trip::where('id', $id)->firstOrFail();

        $trip->vehicleId = $request->input('vehicleId');
        $trip->driverId = $request->input('driverId');
        $trip->customerId = $request->input('customerId');
        $trip->status = $request->input('status');

        $trip->save();

        return $trip;

    }
}
