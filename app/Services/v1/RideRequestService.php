<?php 

namespace App\Services\v1;

use App\request;
use Validator;


class RideRequestService
{


    protected $rules = [
        // 'email' => 'required|string|email|max:255|unique:drivers',
        // 'operatorid'=>'required|integer|unique:operators',
        // 'vehicleId'=> 'required|integer|unique:vehicles',
    ];
    public function validate($request)
    {
        $validator = Validator::make($request, $this->rules);

        if ($validator->fails()) {
            return "error validating user inputs";
        } else {
            $validator->validate();
        }
    }

    public function createTripRequest($request)
    {

        $createTripRequest = request::create([
            'vehicleId' => $request->vehicleId,
            'initial_location_latitude' => $request->initial_location_latitude,
            'initial_location_longitude' => $request->initial_location_longitude,
            'customerId' => $request->customerId,
            'destination_latitude' => $request->destination_latitude,
            'destination_longitude' => $request->destination_longitude,
        ]);

        if ($createTripRequest) {

            $parameters['latitude'] = $createTripRequest .initial_location_latitude;
            $parameters['longitude'] = $createTripRequest .initial_location_longitude;
            // $data = $this->business->getDrivers($parameters);
            // return response()->json($data);

            return $createTripRequest;
        } else {
            return "Oops, unable to register a Trip Request Please try again later";
        }
    }

    public function updateTripRequest($request, $id)
    {
        $tripRequest = request::where('id', $id)->firstOrFail();

        $tripRequest->vehicleId = $request->input('vehicleId');
        $tripRequest->initial_location_latitude = $request->input('initial_location_latitude');
        $tripRequest->initial_location_longitude = $request->input('initial_location_longitude');
        $tripRequest->destination_latitude = $request->input('destination_latitude');
        $tripRequest->destination_longitude = $request->input('destination_longitude');
        $tripRequest->customerId = $request->input('customerId');
        $tripRequest->status = $request->input('status');

        $tripRequest->save();

        return $tripRequest;

    }

}