<?php 

namespace App\Services\v1;

use App\route;
use Validator;

class DriverRouteService
{
    protected $rules = [
        // 'email' => 'required|string|email|max:255|unique:drivers',
        // 'operatorid'=>'required|integer|unique:operators',
        // 'vehicleId'=> 'required|integer|unique:vehicles',
    ];
    public function validate($request)
    {
        $validator = Validator::make($request, $this->rules);

        if ($validator->fails()) {
            return "error validating user inputs";
        } else {
            $validator->validate();
        }
    }
    public function getRoute($id)
    {
        $result = route::where('id', $id)->firstOrFail();
        return $result;
    }

    public function getTripRoute($routeId)
    {
        $result = route::where('tripId', $routeId)->get();
        return $result;
    }

    public function createRoute($request)
    {

        $newRoute = route::create([
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
            'tripId' => $request->tripId,
            'vehicleId' => $request->vehicleId,
        ]);

        if ($newRoute) {
            return $newRoute;
        } else {
            return "Oops, unable to register a route";
        }
    }
} 