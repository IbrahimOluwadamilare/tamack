<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class request extends Model
{
    //

    protected $fillable = [
        'customerId', 'vehicleId', 'initial_location_latitude', 'destination_latitude', 'destination_longitude','initial_location_longitude'
    ];
}
