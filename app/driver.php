<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class driver extends Model
{
    protected $fillable = [
        'firstName', 'lastName', 'email', 'password', 'phoneNo', 'address', 'city', 'state', 'driverId', 'longitude', 'latitude', 'routeId', 'status', 'requestStatus', 'pair', 'rating', 'operatorid', 'vehicleId'
    ];
    
    public function operatordrivers()
    {
        return $this->belongsTo('App\operator');
    }
}
