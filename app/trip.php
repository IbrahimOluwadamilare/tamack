<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trip extends Model
{
    //
    protected $fillable = [
        'vehicleId', 'driverId', 'customerId', 'status',
    ];
    public function customertrip()
    {
        return $this->belongsToMany('App\User');
    }
}
