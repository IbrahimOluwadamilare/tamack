<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cardDb extends Model
{
    //

    protected $fillable = [
         'ccv', 'customerId', 'expiryDate','cardNo'
    ];
    public function card(){
        return $this->belongsTo('App\users');
    }
}
