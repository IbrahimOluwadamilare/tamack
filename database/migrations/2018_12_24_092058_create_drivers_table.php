<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driverId');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('email');
            $table->string('phoneNo');
            $table->string('password');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->float('longitude');
            $table->float('latitude');
            $table->integer('routeId')->nullable();
            $table->string('status');
            $table->boolean('requestStatus')->nullable();
            $table->boolean('pair');
            $table->float('rating')->nullable();
            $table->integer('operatorid');
            $table->integer('vehicleId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
