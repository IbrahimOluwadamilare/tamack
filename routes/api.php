<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Auth\RegisterController@create');

Route::resource('v1/operator', v1\BusinessController::class);

Route::resource('v1/driver' , v1\DriverController::class);

Route::resource('v1/driverroute', v1\DriverRouteController::class);

Route::resource('v1/requestride', v1\RideRequestController::class);

Route::resource('v1/trip', v1\TripController::class);